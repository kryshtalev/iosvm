//
//  AuxManager.h
//  Prodensa
//
//  Created by Alexander Kryshtalev on 01.02.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface AuxManager : NSObject

+ (void)callPhoneNumber:(NSString *)number;
+ (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message;
+ (MFMessageComposeViewController *)composeMessageTo:(NSString *)recipient withMessage:(NSString *)message;
+ (UIViewController *)rootController;

@end
