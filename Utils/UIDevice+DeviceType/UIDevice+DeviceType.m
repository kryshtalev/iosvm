//
//  UIDevice+DeviceType.m
//  NDA
//
//  Created by Dmitry Tihonov on 25.03.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "UIDevice+DeviceType.h"

@implementation UIDevice(DeviceType)

+ (DeviceType)currentDeviceType
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        if ((NSInteger)screenSize.height == 480){
            return DeviceTypeIPhone;
        } else {
            return DeviceTypeIPhone5;
        }
    } else {
        return DeviceTypeIPad;
    }
}

@end
