//
//  UIDevice+DeviceType.h
//  NDA
//
//  Created by Dmitry Tihonov on 25.03.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

enum DeviceType {
    DeviceTypeIPhone    = 0,
    DeviceTypeIPhone5,
    DeviceTypeIPad
};
typedef enum DeviceType DeviceType;

@interface UIDevice(DeviceType)

+ (DeviceType)currentDeviceType;

@end
