//
//  VMBaseRequest.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMEntity.h"

@interface VMBaseRequest : VMEntity
{
	NSString *baseUrl;
}

@property (copy) NSString *method;
@property (copy) NSString *version;
@property (copy) NSString *bundle_id;

- (id)initWithMethod:(NSString *)method;
- (NSString *)getUrl;

@end
