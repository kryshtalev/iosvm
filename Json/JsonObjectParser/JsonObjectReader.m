//
//  JsonObjectReader.m
//  JSONObject
//
//  Created by Alexander Kryshtalev on 24.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "JsonObjectReader.h"
#import "objc/runtime.h"
#import "PropertyUtils.h"
#import "JsonDataProvider.h"
#import "SBJsonParser.h"

@implementation JsonObjectReader

+ (id) readObjectFromJson: (NSString*) json forClass: (id) _class withNode: (NSString*) withNode;
{
	SBJsonParser* parser = [[SBJsonParser alloc] init];
	id parsed =[parser objectWithString:json];
	return [self readObjectFromDictionary:parsed forClass:_class withNode:withNode];
}

+ (id) readObjectFromJson: (NSString*) json forClass: (id) _class
{
	return [self readObjectFromJson:json forClass:_class];
}

+ (id) readObjectFromDictionary: (NSDictionary*) dictionary forClass: (id) _class
{
	return [self readObjectFromDictionary:dictionary forClass:_class withNode:nil];
}

+ (id) readObjectFromDictionary: (NSDictionary*) dictionary forClass: (id) _class withNode: (NSString*) withNode;

{
	NSObject <JsonDataProvider>* result = [[_class alloc] init];
	id parsedJson = (withNode != nil) ? [dictionary objectForKey:withNode] : dictionary;
	
	if ([parsedJson isKindOfClass:[NSArray class]]) {
		[self readArray: result propertyName:withNode arrayItems:parsedJson];
	}
	else if ([parsedJson isKindOfClass:[NSDictionary class]]) {
		NSDictionary* values = parsedJson;
		NSDictionary* properties = [PropertyUtils classAndParentPropsFor: _class];
		
		for (NSString* propertyName in [properties allKeys]) {
			
			
			NSString* propertyType = [properties objectForKey:propertyName];
			//NSLog(@"Property: %@, %@", propertyName, propertyType);
			
			id child = [values objectForKey:propertyName];
			
			if ([child isKindOfClass:([NSArray class])]) {
				[self readArray: result propertyName:propertyName arrayItems:child];
			}
			
			if ([child isKindOfClass:([NSString class])]
				|| [child isKindOfClass:([NSNumber class])])
			{
				[result setValue:child forKey:propertyName];
			}
			
			if ([child isKindOfClass:([NSDictionary class])]) {
				
				id nextObject = [JsonObjectReader readObjectFromDictionary:values forClass:NSClassFromString(propertyType) withNode:propertyName];
				
				[result setValue:nextObject forKey:propertyName];
			}
		}
	}
	
	return result;
}

+ (void) readArray: (NSObject <JsonDataProvider>*) result propertyName: (NSString*)propertyName arrayItems: (NSArray*) arrayItems
{
	NSMutableArray* resultArray = [[NSMutableArray alloc] initWithCapacity: [arrayItems count]];
	id arrayClass = [result arrayClassForProperty: propertyName];
	if (arrayClass != nil) {
		for (id item in arrayItems) {
			id arrayItem = [self readObjectFromDictionary:(NSDictionary *)item forClass:arrayClass];
			[resultArray addObject:arrayItem];
		}
		[result setValue:resultArray forKey:propertyName];
	}
}
	
+ (void) dumpDictionary: (NSDictionary* ) dictionary
{
	for (NSString* key in [dictionary allKeys]) {
		//NSLog(@"%@ = %@", key, [dictionary objectForKey:key]);
	}
}

@end
