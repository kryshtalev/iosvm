//
//  ButtonTabsManagerDelegate.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.10.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VMButtonTabsManagerDelegate <NSObject>

- (void)didChangeSelection: (NSUInteger) index;

@end
