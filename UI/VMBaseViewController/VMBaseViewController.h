//
//  VMBaseViewController.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMWaitingViewController.h"

@interface VMBaseViewController : UIViewController  <UITextFieldDelegate>
{
	VMWaitingViewController *waitingViewController;
    UITextField *activeField;
}

- (UIViewController *)navigateTo: (id)controllerClass;
- (void)navigateBack;
- (void)navigateToHome;

#pragma - Navigation bar

- (void)customNavbarImage: (NSString*)imageName;
- (void)customNavBarTitle: (NSString*)title withFont: (UIFont *)font;
- (UIBarButtonItem*)customUIBarButtonWithImage: (NSString*)imageName forAction: (SEL)action;
- (UIButton *)customNavBarTitleButtonWithImage:(NSString *)imageName selectedImage:(NSString *)selectedImageName forAction:(SEL)action;

#pragma - Error messages

- (void)notImplementedYet;
- (void)showError: (NSString *)error;
- (void)showInfo: (NSString *)error;
// - (void)showWhoops: (NSString *)error;

#pragma - Layout

- (void) replaceView: (UIView*)source withAnother: (UIView*)view;
- (UIViewController *)replaceView: (UIView*)source withController:(id)theClass;
- (UIViewController *)replaceView: (UIView*)source withController:(id)theClass type:(NSString *)animationType subtype:(NSString *)animationSubtype;
- (UIViewController *)addViewController:(id)theClass withAnimationType:(NSString *)animationType andSubType:(NSString *)animationSubtype;
- (void)hideViewController:(UIViewController *)controller type:(NSString *)animationType subtype:(NSString *)animationSubtype;

#pragma - Waiting screen

- (void)showWaitingScreenWithText:(NSString *)text;
- (void)showWaitingScreenOnView:(UIView *)view withText:(NSString *)text;
- (void)hideWaitingScreen;

#pragma - Scroll View

- (void)registerForKeyboardNotifications;
- (void)unregisterForKeyboardNotifications;
- (void)keyboardWasShown:(NSNotification*)aNotification;
- (void)keyboardWillBeHidden:(NSNotification*)aNotification;
- (void)scrollToView:(UIView *)view withNotification:(NSNotification*)aNotification;
- (void)scrollToPoint:(CGPoint)point withNotification:(NSNotification*)aNotification;
- (void)scrollToTop;


@end

