//
//  UICustomFontButton.m
//  NDA
//
//  Created by Alexander Kryshtalev on 19.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "UICustomFontButton.h"

@implementation UICustomFontButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setCustomFont:(NSString *)customFont
{
	CGFloat currentSize = self.titleLabel.font.pointSize;
	self.titleLabel.font = [UIFont fontWithName:customFont size:currentSize];
}

- (NSString *)customFont
{
	return self.titleLabel.font.fontName;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
