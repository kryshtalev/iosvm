//
//  UICustomFontLabel.m
//  NDA
//
//  Created by Alexander Kryshtalev on 19.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "UICustomFontLabel.h"

@implementation UICustomFontLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setCustomFont:(NSString *)customFont
{
	CGFloat currentSize = self.font.pointSize;
	self.font = [UIFont fontWithName:customFont size:currentSize];
}

- (NSString *)customFont
{
	return self.font.fontName;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
